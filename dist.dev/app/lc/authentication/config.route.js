(function() {
    'use strict';

    angular
        .module('app.lc.authentication')
        .config(moduleConfig);
    
    moduleConfig.$inject = ['$stateProvider'];

    /* @ngInject */
    function moduleConfig($stateProvider) {
        $stateProvider
        .state('authentication', {
            abstract: true,
            templateUrl: 'app/lc/authentication/layout/authentication.tmpl.html'
        })
        .state('authentication.login', {
            url: '/login',
            templateUrl: 'app/lc/authentication/login/login.tmpl.html',
            controller: 'LoginController',
            controllerAs: 'lc'
        })
         .state('authentication.registration', {
            url: '/memberRegistration',
            templateUrl: 'app/lc/authentication/registration/member.registration.tmpl.html',
            controller: 'MemberRegistrationController',
            controllerAs: 'mrc'
        })
    }
})();