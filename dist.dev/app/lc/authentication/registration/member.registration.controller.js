(function () {
    'use strict';

    angular
        .module('app.lc.authentication')
        .controller('MemberRegistrationController', MemberRegistrationController);

    MemberRegistrationController.$inject = ['$scope', '$http', 'apiurlconfig','$state'];

    function MemberRegistrationController($scope, $http, apiurlconfig,$state) {
        var mrc = this;

        mrc.memRegistration = function () {
            $http.post(apiurlconfig.memReg, mrc.member).then(function (response) {
                alert('succeeded', response.data);
                $state.go('authentication.login');
            }, function (errorResponse) {
                alert('Failed', errorResponse);
            });
        }
        
        mrc.memRegCancel =function()
         {
        
        	 $state.go('authentication.login');
        	 
         }
   
    }
   

})();