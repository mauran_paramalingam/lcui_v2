(function () {
    'use strict';

    angular
        .module('app.lc')
        .factory('AuthServerProvider', AuthServerProvider);

    AuthServerProvider.$inject = ['$http', 'apiurlconfig', '$q'];

    function AuthServerProvider($http, apiurlconfig, $q) {
        
        function Session() {
            var tempThis = this;
        }
        
        Session.prototype.setSession = function(_session){
            var tempThis = this;
            angular.forEach(_session, function (value, key) {
                tempThis[key] = value;
            });
        }
        
        Session.prototype.getSession = function(){
            var tempThis = this;
            return tempThis;
        }

        function login(credentials) {
            var deffered = $q.defer();
            var promise = deffered.promise;
            var session = new Session();
            $http.post(apiurlconfig.login, credentials).then(function (response) {
                session.setSession(response.data);   
                deffered.resolve(session);
            }, function (errorResponse) {
                deffered.reject();
                console.log("Handle Error Appropriately");
            });
            return promise;
        }

        function getSession() {
            return Session.getSession();
        }
        
        
        return {
            Session : Session,
            login: login
        }

    }
})();