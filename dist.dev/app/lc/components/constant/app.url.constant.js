(function () {
    angular
        .module('app.lc')
        .constant('apiurlconfig', {
            "login": "http://ec2-52-32-213-136.us-west-2.compute.amazonaws.com/lcuser/login/",
            "memReg": "http://ec2-52-32-213-136.us-west-2.compute.amazonaws.com/lcuser/register/",

        //---profile-------------------------------

            "jobtypes": "http://ec2-52-32-213-136.us-west-2.compute.amazonaws.com/jobtypes/",
            "yearsofexperiencecategories": "http://ec2-52-32-213-136.us-west-2.compute.amazonaws.com/yearsofexperiencecategories/",
            "currentemployertypes": "http://ec2-52-32-213-136.us-west-2.compute.amazonaws.com/currentemployertypes/",
            "reasonforlookingcategories": "http://ec2-52-32-213-136.us-west-2.compute.amazonaws.com/reasonforlookingcategories/",
            "profile": "http://ec2-52-32-213-136.us-west-2.compute.amazonaws.com/profile/"
    });
})();