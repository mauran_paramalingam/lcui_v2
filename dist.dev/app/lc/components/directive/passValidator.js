(function () {
    'use strict';
    angular.module("app.lc").directive("validPassword", function () {
        return {
            require: 'ngModel',
            link: function (scope, elm, attrs, ctrl) {
                ctrl.$parsers.unshift(function (viewValue, $scope) {
                    var noMatch = viewValue != scope.memregform.password.$viewValue
                    ctrl.$setValidity('noMatch', !noMatch)
                })
            }
        };
    });
})();