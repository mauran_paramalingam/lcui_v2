(function(){
    'use strict';
    
    angular
        .module('app.lc',['app.lc.authentication',
                         'app.lc.profile']);
})();