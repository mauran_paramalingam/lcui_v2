(function() {
    'use strict';

    angular
        .module('app.lc.profile')
        .config(moduleConfig);
    
    moduleConfig.$inject = ['$stateProvider'];

    /* @ngInject */
    function moduleConfig($stateProvider) {
        $stateProvider
        .state('profile', {
            abstract: true,
            templateUrl: 'app/lc/profile/layout/profile.tmpl.html'
        })
        .state('profile.new', {
            url: '/newProfile',
            templateUrl: 'app/lc/profile/new-profile/new-profile.tmpl.html',
            controller: 'NewProfileController',
            controllerAs: 'npc'
        })
    }
})();