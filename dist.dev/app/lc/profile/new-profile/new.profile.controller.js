(function () {
    'use  strict';
    angular
        .module('app.lc.profile')
        .controller('NewProfileController', NewProfileController);

    NewProfileController.$inject = ['$rootScope','$http','$scope','apiurlconfig'];

    function NewProfileController($rootScope,$http,$scope,apiurlconfig) {
        console.log($rootScope.session);
        var credentials = {};
        credentials.username = "Qf1nBEkCOWjK+3b0rDzVn1E3";
        credentials.password = "w2kpL3D3BVmMdvwmHdU49CX3xRhvxUotdPzS";

        var npc = this;

        npc.yearsOfExperience_error = true;
        npc.currentlyWorkFor_error = true;
        npc.salary_slider = false;
        npc.pbs_slider = false;


        npc.noppQ1 = true;
        npc.noppQ2 = true;
        npc.noppQ3 = true;

        npc.years_of_experience_id = 'Select a value';
        npc.current_employer_type_id = 'Select a value';





        npc.profileUser = {
            /*-----------*/

            user_id: 1,
           
            current_employer: "Employer",
         
            //	state:"",
            zip: '',
            move_timeline_id: '1',
            desired_lender_size_id: '3',
            required_comp_production_bonus: 'false',
            years_of_experience_id: 'Select a value',
            current_employer_type_id: 'Select a value',
            comp_salary: 100000,
            comp_bps: 100000,
            comp_production_bonus: 100000,
            required_comp_bps: 100000,
            required_comp_production_bonus: false,
            required_comp_salary: 100000,
            reason_looking_id: 1,

            /*-----------*/

        };
     

        npc.mobile =function()
        {
        	 	 npc.profileUser.mobile_phone=npc.mobileNumber1 + npc.mobileNumber2 + npc.mobileNumber3;
             	 return npc.profileUser.mobile_phone;
        }

        npc.temp = {
            representsType: ''
        };

        npc.styler = {
            btnColorChange1: 'btn btn-success btn-circle btn-xl1',
            btnColorChange2: 'btn btn-success btn-circle btn-xl1',
            btnColorChange3: 'btn btn-success btn-circle btn-xl1',
            btnColorChange4: 'btn btn-success btn-circle btn-xl1',
            btnColorChange5: 'btn btn-success btn-circle btn-xl1',
            btnColorChange6: 'btn btn-success btn-circle btn-xl1',

            //current comp plan
            btnSalary: 'btn btn-success btn-circle btn-xl',
            btnBps: 'btn btn-success btn-circle btn-xl',
            btnProduction: 'btn btn-success btn-circle btn-xl',

            //yes opp
            btnLackofSupport: 'btn btn-success btn-circle btn-xl',
            btnMoreIncome: 'btn btn-success btn-circle btn-xl',
            btnConfidenceinOperations: 'btn btn-success btn-circle btn-xl',
            btnBetterOpportunity: 'btn btn-success btn-circle btn-xl',
            btnLeadership: 'btn btn-success btn-circle btn-xl',
            btnClosingTimeline: 'btn btn-success btn-circle btn-xl',
            btnCantgrowmyteam: 'btn btn-success btn-circle btn-xl',



        };


        //step 1 



        //-------------------------------------------------------------------------------------------------------------------

        npc.jobTypes = function () {
            npc.jobTypeSelection = {};

            var authdata = setUpLogin(credentials);
            $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata;
            $http.get(apiurlconfig.jobtypes, {}).then(function (response) {
                npc.jobTypeSelection = response.data.objects;
            });
        }

        npc.jobTypes();




        npc.selectedJobTypes = function (jobType) {
            npc.profileUser.job_type_id = jobType.id;


            if (npc.profileUser.job_type_id == 1) {

                $scope.setCurrentStep(1);
            }


            console.log(npc.profileUser);
        }



        //--------------------------------------------------
        /*	npc.goToLoanOfficer = function() {
        		npc.profileUser.job_type_id = 1;
        		npc.temnpc.representsType='Loan Officer';
        		console.log(npc.profileUser);
        	}*/
        //-------------------------------------------------------


        //Loan Officer 
        //---------------------------------------------------------------
        //--yearsOfExperience

        npc.loadYearsOfExp = function () {
            npc.yearsOfExpDropdown = {};

            var authdata = setUpLogin(credentials);
            $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata;
            $http.get(apiurlconfig.yearsofexperiencecategories, {}).then(function (response) {
                npc.yearsOfExpDropdown = response.data.objects;
            });
        }

        npc.loadYearsOfExp();

        npc.exYears = function (_year) {
            npc.years_of_experience_id = _year.years_of_experience;
            npc.profileUser.years_of_experience_id = _year.id;
            console.log(npc.profileUser);
        }

        //--------------------------------------------------
        //---currentlyWorkFor



        npc.currentemployertypes = function () {
            npc.curEmptypesDropdown = {};
            var authdata = setUpLogin(credentials);
            $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata;
            $http.get(apiurlconfig.currentemployertypes, {}).then(function (response) {
                npc.curEmptypesDropdown = response.data.objects;
            });
        }

        npc.currentemployertypes();


        npc.currentEmpTy = function (empType) {
            npc.current_employer_type_id = empType.current_employer_type;
            npc.profileUser.current_employer_type_id = empType.id;
            console.log(npc.profileUser);
        }






        //--------------------------------------------------




        //---------------------------------------------------------------------------


        //looking
        //---------------------------------------------------------------------------
        npc.opportunityNo = function () {
            npc.profileUser.looking_for_opportunity = 'false';
            console.log(npc.profileUser);
        }

        npc.opportunityYes = function () {
                npc.profileUser.looking_for_opportunity = 'true';
                console.log(npc.profileUser);
            }
            //---------------------------------------------------------------------------


        //No Opp
        //---------------------------------------------------------------------------
        npc.profileVisibleYes = function () {
            npc.noppQ1 = false;
            npc.styler.btnColorChange1 = 'btn btn-circle btn-xl1 selected',
                npc.styler.btnColorChange2 = 'btn btn-success btn-circle btn-xl1',
                npc.profileUser.profile_visibility = 'true';

            console.log(npc.profileUser);
        }
        npc.profileVisibleNo = function () {
            npc.noppQ1 = false;
            npc.styler.btnColorChange2 = 'btn btn-circle btn-xl1 selected',
                npc.styler.btnColorChange1 = 'btn btn-success btn-circle btn-xl1',
                npc.profileUser.profile_visibility = 'false';
            console.log(npc.profileUser);
        }


        npc.acceptsOffersYes = function () {
            npc.noppQ2 = false;
            npc.styler.btnColorChange3 = 'btn btn-circle btn-xl1 selected',
                npc.styler.btnColorChange4 = 'btn btn-success btn-circle btn-xl1',
                npc.profileUser.accept_offers = 'true';
            npc.profileUser.accept_communications = 'true';
            console.log(npc.profileUser);
        }
        npc.acceptsOffersNo = function () {
            npc.noppQ2 = false;
            npc.styler.btnColorChange4 = 'btn btn-circle btn-xl1 selected',
                npc.styler.btnColorChange3 = 'btn btn-success btn-circle btn-xl1',
                npc.profileUser.accept_offers = 'false';
            npc.profileUser.accept_communications = 'false';
            console.log(npc.profileUser);
        }

        npc.namePrivateYes = function () {
            npc.noppQ3 = false;
            npc.styler.btnColorChange5 = 'btn btn-circle btn-xl1 selected',
                npc.styler.btnColorChange6 = 'btn btn-success btn-circle btn-xl1',
                npc.profileUser.name_private = 'true';
            console.log(npc.profileUser);
        }
        npc.namePrivateNo = function () {
                npc.noppQ3 = false;
                npc.styler.btnColorChange6 = 'btn btn-circle btn-xl1 selected',
                    npc.styler.btnColorChange5 = 'btn btn-success btn-circle btn-xl1',
                    npc.profileUser.name_private = 'false';
                console.log(npc.profileUser);
            }
            //---------------------------------------------------------------------------

        //half way done
        //---------------------------------------------------------------------------
        npc.saveLoanOfficer = function () {

            var authdata = setUpLogin(credentials);
            $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata;
            $http.post(apiurlconfig.profile, npc.profileUser).then(function (response) {
                alert(' success', response);
                // loginpanel=false;
                //    successpanel=true;

                //	console.log(npc.profileUser);

            }, function (errorResponse) {

                alert('Failed', errorResponse);
                //   l.loginError=true;
            });
        }

        function setUpLogin(credentials) {
            credentials.username = "Qf1nBEkCOWjK+3b0rDzVn1E3";
            credentials.password = "w2kpL3D3BVmMdvwmHdU49CX3xRhvxUotdPzS";
            var authdata = btoa(credentials.username + ':' + credentials.password);
            return authdata;
        }









        //---------------------------------------------------------------------------


        //Detailed Questions
        //----------------------------------------------------------------------------

        //----Do you have a Production or support team
        //-------------------------------------
        var user1Data = [{
            id: 1,
            certifications: 'AMP',

	}, {
            id: 2,
            certifications: 'CMB',

	}, {
            id: 3,
            certifications: 'CMB - Executive',

	}, {
            id: 4,
            certifications: 'CMP -Master',

	}, {
            id: 5,
            certifications: 'CMPS',

	}, {
            id: 6,
            certifications: 'CRMS',

	}, {
            id: 7,
            certifications: 'CMC',

	}, {
            id: 8,
            certifications: 'GMA',

	}, {
            id: 9,
            certifications: 'Other',

	}];

        // init
        $scope.test345 = 'blue';
        $scope.selectedA = {};
        $scope.selectedB = {};

        $scope.listA = user1Data.slice(0, 10);
        $scope.listB = user1Data.slice(0, 0);
        $scope.items = user1Data;

        $scope.checkedA = false;
        $scope.checkedB = false;

        function arrayObjectIndexOf(myArray, searchTerm, property) {
            for (var i = 0, len = myArray.length; i < len; i++) {
                if (myArray[i][property] === searchTerm)
                    return i;
            }
            return -1;
        }

        $scope.aToB = function () {

            for (var i in $scope.selectedA) {

                if ($scope.selectedA.hasOwnProperty(i)) {
                    var moveId = arrayObjectIndexOf($scope.items, $scope.selectedA[i],
                        "id");
                    $scope.listB.push($scope.items[moveId]);
                    var delId = arrayObjectIndexOf($scope.listA, $scope.selectedA[i],
                        "id");
                    $scope.listA.splice(delId, 1);

                }

            }
            reset();
        };

        $scope.bToA = function () {
            for (i in $scope.selectedB) {
                if ($scope.selectedB.hasOwnProperty(i)) {
                    var moveId = arrayObjectIndexOf($scope.items, $scope.selectedB[i],
                        "id");
                    $scope.listA.push($scope.items[moveId]);
                    var delId = arrayObjectIndexOf($scope.listB, $scope.selectedB[i],
                        "id");
                    $scope.listB.splice(delId, 1);
                }
            }
            reset();
        };

        function reset() {
            $scope.selectedA = [];
            $scope.selectedB = [];
            //$scope.toggle = 0;
        }

        $scope.toggleA = function () {

            if ($scope.selectedA.length > 0) {
                $scope.selectedA = [];
                //$scope.test345='green';
            } else {
                for (i in $scope.listA) {
                    $scope.selectedA.push($scope.listA[i].id);

                }
            }
        }

        $scope.toggleB = function () {

            if ($scope.selectedB.length > 0) {
                $scope.selectedB = {};
            } else {
                for (i in $scope.listB) {
                    $scope.selectedB.push($scope.listB[i].id);
                }
            }
        }


        $scope.selectA = function (i) {

            if ($scope.selectedA.hasOwnProperty(i)) {

                delete $scope.selectedA[i];
                console.log("removed =" + i)
            } else {

                $scope.selectedA[i] = i;
                console.log("added =" + i)
            }

        };

        $scope.selectB = function (i) {

            if ($scope.selectedB.hasOwnProperty(i)) {

                delete $scope.selectedB[i];
                console.log("removed =" + i)
            } else {

                $scope.selectedB[i] = i;
                console.log("added =" + i)
            }

        };





        //-------------------------------------

        //----Are you a member of the following 

        var user2Data = [{
            id: 1,
            member_organization: 'The CORE',

}, {
            id: 2,
            member_organization: 'Building Champsion',

}, {
            id: 3,
            member_organization: 'Mortgage Marketing Animals',

}, {
            id: 4,
            member_organization: 'NAMB',

}, {
            id: 5,
            member_organization: 'Mortgage Mastery Club',

}, {
            id: 6,
            member_organization: 'Other-Not listed',

}];

        function arrayObjectIndexOf(myArray, searchTerm, property) {
            for (var t = 0, len = myArray.length; t < len; t++) {
                if (myArray[t][property] === searchTerm)
                    return t;
            }
            return -1;
        }

        $scope.selectedC = {};
        $scope.selectedD = {};

        $scope.listC = user2Data.slice(0, 10);
        $scope.listD = user2Data.slice(0, 0);
        $scope.items1 = user2Data;

        $scope.checkedC = false;
        $scope.checkedD = false;


        $scope.cToD = function () {
            //debugger;
            for (t in $scope.selectedC) {
                if ($scope.selectedC.hasOwnProperty(t)) {
                    var moveId1 = arrayObjectIndexOf($scope.items1, $scope.selectedC[t],
                        "id");
                    $scope.listD.push($scope.items1[moveId1]);
                    var delId1 = arrayObjectIndexOf($scope.listC, $scope.selectedC[t],
                        "id");
                    $scope.listC.splice(delId1, 1);
                }
            }
            reset1();
        };

        $scope.dToC = function () {
            for (t in $scope.selectedD) {
                if ($scope.selectedD.hasOwnProperty(t)) {
                    var moveId1 = arrayObjectIndexOf($scope.items1, $scope.selectedD[t],
                        "id");
                    $scope.listC.push($scope.items1[moveId1]);
                    var delId1 = arrayObjectIndexOf($scope.listD, $scope.selectedD[t],
                        "id");
                    $scope.listD.splice(delId1, 1);
                }
            }
            reset1();
        };

        function reset1() {
            $scope.selectedC = [];
            $scope.selectedD = [];
            //$scope.toggle = 0;
        }

        $scope.toggleC = function () {

            if ($scope.selectedC.length > 0) {
                $scope.selectedC = [];
                $scope.test345 = 'green';
            } else {
                for (t in $scope.listC) {
                    $scope.selectedC.push($scope.listC[t].id);

                }
            }
        }

        $scope.toggleD = function () {

            if ($scope.selectedD.length > 0) {
                $scope.selectedD = [];
            } else {
                for (t in $scope.listD) {
                    $scope.selectedD.push($scope.listD[t].id);
                }
            }
        }

        $scope.selectC = function (t) {

            if ($scope.selectedC.hasOwnProperty(t)) {

                delete $scope.selectedC[t];
                console.log("removed =" + t)
            } else {

                $scope.selectedC[t] = t;
                console.log("added =" + t)
            }

        };

        $scope.selectD = function (t) {

            if ($scope.selectedD.hasOwnProperty(t)) {

                delete $scope.selectedD[t];
                console.log("removed =" + t)
            } else {

                $scope.selectedD[t] = t;
                console.log("added =" + t)
            }

        };




        //-------------------------------------
        //------------------------------------------------------------------------------------------------------------

        //---- yes opp

        /*npc.styler.btnLackofSupport
        npc.styler.btnMoreIncome
        npc.styler.btnConfidenceinOperations
        npc.styler.btnBetterOpportunity
        npc.styler.btnLeadership
        npc.styler.btnClosingTimeline
        npc.styler.btnCantgrowmyteam

        */






        npc.reasonforlookingCategories = function () {
            npc.reasonforlookingSelection = {};
            var authdata = setUpLogin(credentials);
            $http.defaults.headers.common['Authorization'] = 'Basic ' + authdata;
            $http.get(apiurlconfig.reasonforlookingcategories, {}).then(function (response) {
                npc.reasonforlookingCategories = response.data.objects;
            });
        }

        npc.reasonforlookingCategories();


        npc.reasonforlooking = function (lookingTypes) {

            npc.profileUser.reason_looking_id = lookingTypes.id;
            console.log(npc.profileUser);
        }






        npc.selectedE2 = {};

        npc.selectE2 = function (lookingTypes) {

            if (npc.selectedE2.hasOwnProperty(lookingTypes.id)) {

                delete npc.selectedE2[lookingTypes.id];
                console.log("removed =" + lookingTypes.id)
            } else {

                npc.selectedE2[lookingTypes.id] = lookingTypes.id;
                console.log("added =" + lookingTypes.id)
            }

            console.log(npc.selectedE2);
        };









        //------------------------------------------------------------------------------------------------------------

        //----Current comp plan

        npc.salaryClick = function () {

            if (npc.styler.btnSalary == 'btn btn-success btn-circle btn-xl') {
                npc.styler.btnSalary = 'btn btn-warning btn-circle btn-xl';
                npc.salary_slider = true;
                npc.profileUser.comp_salary = true;
                console.log(npc.profileUser);
            } else {
                npc.styler.btnSalary = 'btn btn-success btn-circle btn-xl';
                npc.salary_slider = false;
                npc.profileUser.comp_salary = false;
                console.log(npc.profileUser);
            }

        }

        npc.bpsClick = function () {

            if (npc.styler.btnBps == 'btn btn-success btn-circle btn-xl') {


                npc.styler.btnBps = 'btn btn-warning btn-circle btn-xl';
                npc.bps_slider = true;
                npc.profileUser.comp_bps = true;
                console.log(npc.profileUser);
            } else {
                npc.styler.btnBps = 'btn btn-success btn-circle btn-xl';
                npc.bps_slider = false;
                npc.profileUser.comp_bps = false;
                console.log(npc.profileUser);
            }
        }

        npc.productionClick = function () {
            if (npc.styler.btnProduction == 'btn btn-success btn-circle btn-xl') {
                npc.styler.btnProduction = 'btn btn-warning btn-circle btn-xl';
                npc.profileUser.comp_production_bonus = true;
                console.log(npc.profileUser);
            } else {
                npc.styler.btnProduction = 'btn btn-success btn-circle btn-xl';
                npc.profileUser.comp_production_bonus = false;
            }
        }









        //------------------------------------------------------------------------------------------------------------

        //--- System you use 


        //---CRM you use--------------

        var user3Data = [{
            id: 1,
            crm_youuse: 'None',

}, {
            id: 2,
            crm_youuse: 'Top of mind / Surefire',

}, {
            id: 3,
            crm_youuse: 'vantage',

}, {
            id: 4,
            crm_youuse: 'Jungo/MPC',

}, {
            id: 5,
            crm_youuse: 'Bntouch',

}, {
            id: 6,
            crm_youuse: 'Whiteboard',

}];

        function arrayObjectIndexOf(myArray, searchTerm, property) {
            for (var m = 0, len = myArray.length; m < len; m++) {
                if (myArray[m][property] === searchTerm)
                    return m;
            }
            return -1;
        }

        npc.selectedE = {};
        npc.selectedF = {};

        npc.listE = user3Data.slice(0, 10);
        npc.listF = user3Data.slice(0, 0);
        npc.items2 = user3Data;

        npc.checkedE = false;
        npc.checkedF = false;


        npc.eToF = function () {
            //debugger;
            for (m in npc.selectedE) {
                if (npc.selectedE.hasOwnProperty(m)) {
                    var moveId2 = arrayObjectIndexOf(npc.items2, npc.selectedE[m],
                        "id");
                    npc.listF.push(npc.items2[moveId2]);
                    var delId2 = arrayObjectIndexOf(npc.listE, npc.selectedE[m],
                        "id");
                    npc.listE.splice(delId2, 1);
                }
            }
            reset2();
        };

        npc.fToE = function () {

            for (m in npc.selectedF) {
                if (npc.selectedF.hasOwnProperty(m)) {
                    var moveId2 = arrayObjectIndexOf(npc.items2, npc.selectedF[m],
                        "id");
                    npc.listE.push(npc.items2[moveId2]);
                    var delId2 = arrayObjectIndexOf(npc.listF, npc.selectedF[m],
                        "id");
                    npc.listF.splice(delId2, 1);
                }
            }
            reset2();
        };

        function reset2() {
            npc.selectedE = [];
            npc.selectedF = [];
            //$scope.toggle = 0;
        }


        npc.selectE = function (m) {

            if (npc.selectedE.hasOwnProperty(m)) {

                delete npc.selectedE[m];
                console.log("removed =" + m)
            } else {

                npc.selectedE[m] = m;
                console.log("added =" + m)
            }

        };

        npc.selectF = function (m) {

            if (npc.selectedF.hasOwnProperty(m)) {

                delete npc.selectedF[m];
                console.log("removed =" + m)
            } else {

                npc.selectedF[m] = m;
                console.log("added =" + m)
            }

        };




        //-------------------------------------

        //-----Pricing Engine----------

        //----------------------------

        var user4Data = [{
            id: 1,
            pricing_Engine: 'None',

}, {
            id: 2,
            pricing_Engine: 'Opitimal Blue',

}, {
            id: 3,
            pricing_Engine: 'LoanLogics',

}, {
            id: 4,
            pricing_Engine: 'Encompass PPE',

}, {
            id: 5,
            pricing_Engine: 'Mortech',

}, {
            id: 6,
            pricing_Engine: 'LoanXEngine',

}, {
            id: 7,
            pricing_Engine: 'Other',

}];

        function arrayObjectIndexOf(myArray, searchTerm, property) {
            for (var n = 0, len = myArray.length; n < len; n++) {
                if (myArray[n][property] === searchTerm)
                    return n;
            }
            return -1;
        }

        npc.selectedG = {};
        npc.selectedH = {};

        npc.listG = user4Data.slice(0, 10);
        npc.listH = user4Data.slice(0, 0);
        npc.items3 = user4Data;

        npc.checkedG = false;
        npc.checkedH = false;


        npc.gToH = function () {
            debugger;
            for (n in npc.selectedG) {
                if (npc.selectedG.hasOwnProperty(n)) {
                    var moveId3 = arrayObjectIndexOf(npc.items3, npc.selectedG[n],
                        "id");
                    npc.listH.push(npc.items3[moveId3]);
                    var delId3 = arrayObjectIndexOf(npc.listG, npc.selectedG[n],
                        "id");
                    npc.listG.splice(delId3, 1);
                }
            }
            reset3();
        };

        npc.hToG = function () {

            for (n in npc.selectedH) {
                if (npc.selectedH.hasOwnProperty(n)) {
                    var moveId3 = arrayObjectIndexOf(npc.items3, npc.selectedH[n],
                        "id");
                    npc.listG.push(npc.items3[moveId3]);
                    var delId3 = arrayObjectIndexOf(npc.listH, npc.selectedH[n],
                        "id");
                    npc.listH.splice(delId3, 1);
                }
            }
            reset3();
        };

        function reset3() {
            npc.selectedG = [];
            npc.selectedH = [];
            //$scope.toggle = 0;
        }


        npc.selectG = function (n) {
            //debugger;
            if (npc.selectedG.hasOwnProperty(n)) {

                delete npc.selectedG[n];
                console.log("removed =" + n)
            } else {

                npc.selectedG[n] = n;
                console.log("added =" + n)
            }

        };

        npc.selectH = function (n) {

            if (npc.selectedH.hasOwnProperty(n)) {

                delete npc.selectedH[n];
                console.log("removed =" + n)
            } else {

                npc.selectedH[n] = n;
                console.log("added =" + n)
            }

        };

        //----------------------------------------------

        //----------LOS

        var user5Data = [{
            id: 1,
            los: 'Encompass',

}, {
            id: 2,
            los: 'Calyx Point',

}, {
            id: 3,
            los: 'Byte',

}, {
            id: 4,
            los: 'Mortgage Builder',

}, {
            id: 5,
            los: 'Lending QB',

}, {
            id: 6,
            los: 'OpenClose',

}, {
            id: 7,
            los: 'E3',

}];

        function arrayObjectIndexOf(myArray, searchTerm, property) {
            for (var s = 0, len = myArray.length; s < len; s++) {
                if (myArray[s][property] === searchTerm)
                    return s;
            }
            return -1;
        }

        npc.selectedI = {};
        npc.selectedJ = {};

        npc.listI = user5Data.slice(0, 10);
        npc.listJ = user5Data.slice(0, 0);
        npc.items5 = user5Data;

        npc.checkedI = false;
        npc.checkedJ = false;


        npc.iToJ = function () {

            for (s in npc.selectedI) {
                if (npc.selectedI.hasOwnProperty(s)) {
                    var moveId5 = arrayObjectIndexOf(npc.items5, npc.selectedI[s],
                        "id");
                    npc.listJ.push(npc.items5[moveId5]);
                    var delId5 = arrayObjectIndexOf(npc.listI, npc.selectedI[s],
                        "id");
                    npc.listI.splice(delId5, 1);
                }
            }
            reset5();
        };








        npc.jToI = function () {

            for (s in npc.selectedJ) {
                if (npc.selectedJ.hasOwnProperty(s)) {
                    var moveId5 = arrayObjectIndexOf(npc.items5, npc.selectedJ[s],
                        "id");
                    npc.listI.push(npc.items5[moveId5]);
                    var delId5 = arrayObjectIndexOf(npc.listJ, npc.selectedJ[s],
                        "id");
                    npc.listJ.splice(delId5, 1);
                }
            }
            reset5();
        };

        function reset5() {
            npc.selectedI = [];
            npc.selectedJ = [];
            //$scope.toggle = 0;
        }


        npc.selectI = function (s) {
            //debugger;
            if (npc.selectedI.hasOwnProperty(s)) {

                delete npc.selectedI[s];
                console.log("removed =" + s)
            } else {

                npc.selectedI[s] = s;
                console.log("added =" + s)
            }

        };

        npc.selectJ = function (s) {

            if (npc.selectedJ.hasOwnProperty(s)) {

                delete npc.selectedJ[s];
                console.log("removed =" + s)
            } else {

                npc.selectedJ[s] = s;
                console.log("added =" + s)
            }

        };
        //------------------------------------------------------------------------------------------------------------


        //-------- State and Business-----------------------------------------------------------------------

        //-------States you lend in



        var user6Data = [{
            id: 1,
            states_youLend: 'test1',

}, {
            id: 2,
            states_youLend: 'test2',

}, {
            id: 3,
            states_youLend: 'test3',

}, {
            id: 4,
            states_youLend: 'test4',

}, {
            id: 5,
            states_youLend: 'test5',

}, {
            id: 6,
            states_youLend: 'test6',

}, {
            id: 7,
            states_youLend: 'test7',

}];

        function arrayObjectIndexOf(myArray, searchTerm, property) {
            for (var x = 0, len = myArray.length; x < len; x++) {
                if (myArray[x][property] === searchTerm)
                    return x;
            }
            return -1;
        }

        npc.selectedK = {};
        npc.selectedL = {};

        npc.listK = user6Data.slice(0, 10);
        npc.listL = user6Data.slice(0, 0);
        npc.items6 = user6Data;

        npc.checkedK = false;
        npc.checkedL = false;


        npc.kToL = function () {

            for (x in npc.selectedK) {
                if (npc.selectedK.hasOwnProperty(x)) {
                    var moveId6 = arrayObjectIndexOf(npc.items6, npc.selectedK[x],
                        "id");
                    npc.listL.push(npc.items6[moveId6]);
                    var delId6 = arrayObjectIndexOf(npc.listK, npc.selectedK[x],
                        "id");
                    npc.listK.splice(delId6, 1);
                }
            }
            reset6();
        };








        npc.lToK = function () {

            for (x in npc.selectedL) {
                if (npc.selectedL.hasOwnProperty(x)) {
                    var moveId6 = arrayObjectIndexOf(npc.items5, npc.selectedL[x],
                        "id");
                    npc.listK.push(npc.items6[moveId6]);
                    var delId6 = arrayObjectIndexOf(npc.listL, npc.selectedL[x],
                        "id");
                    npc.listL.splice(delId6, 1);
                }
            }
            reset6();
        };

        function reset6() {
            npc.selectedK = [];
            npc.selectedL = [];
            //$scope.toggle = 0;
        }


        npc.selectK = function (x) {
            //debugger;
            if (npc.selectedK.hasOwnProperty(x)) {

                delete npc.selectedK[x];
                console.log("removed =" + x)


            } else {

                npc.selectedK[x] = x;
                console.log("added =" + x)

            }

        };









        npc.selectL = function (x) {

            if (npc.selectedL.hasOwnProperty(x)) {

                delete npc.selectedL[x];
                console.log("removed =" + x)
            } else {

                npc.selectedL[x] = x;
                console.log("added =" + x)
            }

        };




        npc.toggleK = function () {

            if (npc.selectedK.length > 0) {
                npc.selectedK = [];
                //$scope.test345='green';
            } else {
                for (x in npc.listK) {
                    npc.selectedK[x] = (npc.listK[x].id);

                }
            }
        }

        npc.slider1 = 0;
        npc.slider2 = 0;
        npc.slider3 = 0;
        npc.slider4 = 0;
        npc.slider5 = 0;
        npc.slider6 = 0;
        npc.slider7 = 0;

        npc.whereBusniessCome = function (num1, num2, num3, num4, num5, num6, num7) {
            npc.test322 = parseInt(num1) + parseInt(num2) + parseInt(num3) + parseInt(num4) + parseInt(num5) + parseInt(num6) + parseInt(num7);
            console.log(npc.profileUser);
        }




        /*.whereBusniessCome= function(num1,num2) {
        	npc.test322 = parseInt(num1) + parseInt(num2);
        	console.log(npc.profileUser);
        }
        */







        //------------------------------------------------------------------------------------------------------------



        $scope.steps = ['step1', 'nmls', 'loanOfficer', 'looking', 'step5', 'noOpp', 'halfwaydone', 'detailedQuestions', 'yesOpp', 'currentComp', 'systemYouUse', 'stateAndBusiness'];
        $scope.step = 0;

        $scope.isCurrentStep = function (step) {
            return $scope.step === step;
        };

        $scope.setCurrentStep = function (step) {
            $scope.step = step;
        };

        $scope.getCurrentStep = function () {
            return $scope.steps[$scope.step];
        };


    }
})();