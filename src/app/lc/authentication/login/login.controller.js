(function(){
    'use strict';
    
    angular
        .module('app.lc.authentication')
        .controller('LoginController',LoginController);
    
    LoginController.$inject = ['Auth','$rootScope','$state'];
    
    function LoginController(Auth,$rootScope,$state){
        var lc = this;	    
        lc.credentials = {};
        
        
        lc.login = function(){
            Auth.login(lc.credentials).then(function(){
                //if(typeof $rootScope.previousStateName != "undefined" && $rootScope.previousStateName!='authentication.login'){
                  //  $state.go($rootScope.previousStateName);
                //}else{
                    $state.go('profile.new');
                //}
            });
        }
        
    }
})();