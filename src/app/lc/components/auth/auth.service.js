(function(){
    'use strict';

    angular
        .module('app.lc')
        .factory('Auth',Auth);

    Auth.$inject = ['$rootScope', '$state', '$q', 'AuthServerProvider'];

    function Auth($rootScope, $state, $q, AuthServerProvider){
        return {
            login: function (credentials) {
                var deferred = $q.defer();

                AuthServerProvider.login(credentials).then(function (_session) {
                    $rootScope.session = _session.getSession();
                    deferred.resolve();
                }).catch(function (err) {
                    deferred.reject(err);
                });
                return deferred.promise;
            }
        };
    }
})();