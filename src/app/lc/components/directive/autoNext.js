(function () {
    'use strict';
    angular.module('app.lc').directive('autoNext', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attr, form) {
                var tabindex = parseInt(attr.tabindex);
                var maxLength = parseInt(attr.ngMaxlength);
                element.on('keypress', function (e) {
                    if (element.val().length > maxLength - 2) {
                        //var next = angular.element(document.body).find('textarea[tabindex=' + (tabindex+1) + ']');
                        var next = document.getElementById("text_" + (tabindex + 1));

                        if (next.value.length == 0) {
                            next.focus();
                            return next.triggerHandler('keypress', {
                                which: e.which
                            });
                        } else {
                            return false;
                        }
                    }
                    return true;
                });

            }
        }
    });

})();